<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <script type="text/javascript" src="jquery-1.2.6.min.js"></script>
    <title>Save images!</title>
</head>
<body>
<h3> Max size : 1MB </h3>
<form:form method="post" enctype="multipart/form-data"
           modelAttribute="uploadedFile" action="/imagesaver/">
    <table>
        <tr>
            <td>Upload Image:</td>
            <td><input type="file" name="file" accept="image/*"/>
            </td>
            <td style="color: red; font-style: italic;"><form:errors
                    path="file"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Upload"/>
            </td>
            <td></td>
        </tr>

        <c:if test="${link ne null}">
            <tr>
                <td>Image link:</td>
                <td><a href="${link}">${link}</a>
                </td>
                <td></td>
            </tr>
        </c:if>
    </table>
</form:form>


</body>
</html>  