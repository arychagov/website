<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ошибка!</title>
</head>
<body>
<div id="error">
    <p>${message}</p>
    <a href="${link}">Назад</a>
</div>
</body>
</html>