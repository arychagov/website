<?xml version="1.0" encoding="UTF-8"?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
        Anton Rychagov
    </title>
    <style type="text/css">
        body {
            background: url(./images/goaway.jpg) repeat;
        }

        img {
            width: 11%;
        }

        h1 {
            font-size: 75px;
            color: #ff00ff
        }

        #social_bar {
            position: fixed;
            background-color: white;
            width: 100%;
            bottom: 0;
            left: 0;
            display: inline-table
        }
    </style>
</head>

<body>
<script type="text/javascript">
    function get_random_color() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.round(Math.random() * 15)];
        }
        return color;
    }

    function changeTxtColor() {
        document.getElementById("h").style.color = get_random_color();
    }

    function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }

    var timer = setInterval(changeTxtColor, 5);
</script>

<h1 id="h">Когда-нибудь я перестану быть ленивой жопой и здесь появится какой-то контент</h1>

<div id="social_bar">
    <!--4sq.jpg  gmail.png  goaway.jpg  gp.png  insta.jpg  lastfm.png  skype.png  twitter.jpeg  vk.jpeg-->
    <img src="/images/vk.jpeg" onclick="openInNewTab('http://vk.com/rychagov')"/>
    <img src="/images/4sq.jpg" onclick="openInNewTab('https://foursquare.com/arychagov')"/>
    <img src="/images/insta.jpg" onclick="openInNewTab('http://instagram.com/arychagov')"/>
    <img src="/images/lastfm.png" onclick="openInNewTab('http://www.lastfm.ru/user/MrHankey_')"/>
    <img src="/images/skype.png" onclick="openInNewTab('skype:anton.rychagov?call')"/>
    <img src="/images/twitter.jpeg" onclick="openInNewTab('http://twitter.com/arychagov')"/>
    <img src="/images/facebook.png" onclick="openInNewTab('https://www.facebook.com/anton.rychagov')"/>
    <img src="/images/gp.png" onclick="openInNewTab('https://plus.google.com/u/0/+AntonRychagov/')"/>
    <img src="/images/gmail.png" onclick="openInNewTab('mailto:anton.rychagov@gmail.com')"/>
</div>
</body>
</html>
