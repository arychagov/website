package ru.rychagov.web;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.rychagov.web.file.FileValidator;
import ru.rychagov.web.file.UploadedFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * User: Anton Rychagov (ram)
 * Date: 16.11.13 - 15:45
 */
@Controller
@RequestMapping(value = "/imagesaver/")
public class ImageSaverController {
    @Autowired
    FileValidator fileValidator;

    public static final int TRY_COUNT_MAX = 50;
    public static final char[] LETTERS = ("qwertyuiopasdfghjklzxcvbnm1234567890" + "qwertyuiopasdfghjklzxcvbnm".toUpperCase()).toCharArray();

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView imageSaver(HttpServletRequest request) {
        return new ModelAndView("image");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView fileUploaded(
            @ModelAttribute("uploadedFile") UploadedFile uploadedFile,
            BindingResult result) {

        InputStream inputStream = null;
        OutputStream outputStream = null;

        MultipartFile file = uploadedFile.getFile();
        fileValidator.validate(uploadedFile, result);

        String fileName = getRandString() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));

        if (result.hasErrors()) {
            return new ModelAndView("uploadForm");
        }

        try {
            inputStream = file.getInputStream();

            File newFile = new File(ShowImageController.UPLOAD_DIRECTORY + fileName);
            File dir = new File(ShowImageController.UPLOAD_DIRECTORY);

            if (!dir.exists()) {
                dir.mkdirs();
            }

            int tryCount = 0;
            while (newFile.exists() && tryCount < TRY_COUNT_MAX) {
                tryCount++;
                newFile = new File(ShowImageController.UPLOAD_DIRECTORY + fileName);
            }

            if (tryCount == TRY_COUNT_MAX) {
                return new ModelAndView("error").addObject("message", "Unable to create file, try again").addObject("link", "../imagesaver/");
            }

            System.out.println("Creating new file " + fileName);

            newFile.createNewFile();

            outputStream = new FileOutputStream(newFile);
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            outputStream.close();
            inputStream.close();

            return new ModelAndView("image").addObject("link", generateLink(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            return new ModelAndView("error").addObject("message", "Exception occured" + e.getClass().getSimpleName()).addObject("link", "../imagesaver/");
        }
    }

    private String getRandString() {
        return RandomStringUtils.random(10, 0, LETTERS.length - 1, true, true, LETTERS);
    }

    private String generateLink(String fileName) {
        return "http://arychagov.ru/img/" + fileName;
    }
}
