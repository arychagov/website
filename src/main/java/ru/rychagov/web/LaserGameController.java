package ru.rychagov.web;

import com.sun.istack.internal.NotNull;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * User: arychagov@
 * Company: Yandex
 * Date: 27.11.13 - 13:03
 * Description: <br/>
 */
@Controller
@RequestMapping(value = "/laser")
public class LaserGameController {
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    private ResponseEntity<String> getLaserInfo(@NotNull final HttpServletRequest request) {
        try {
            final File laserInfoFile = new File("/home/ram/lasers/info.xml");

            final String xml = IOUtils.toString(new FileInputStream(laserInfoFile));

            if (xml != null) {
                return new ResponseEntity<String>(xml, HttpStatus.OK);
            } else {
                return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/level/{filename:.+}", method = RequestMethod.GET)
    private ResponseEntity<String> getLaserLevel(@NotNull final HttpServletRequest request, @NotNull @PathVariable(value = "filename") final String filename) {
        try {
            final File laserInfoFile = new File("/home/ram/lasers/levels/" + filename);

            final String xml = IOUtils.toString(new FileInputStream(laserInfoFile));

            if (xml != null) {
                return new ResponseEntity<String>(xml, HttpStatus.OK);
            } else {
                return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
    }
}