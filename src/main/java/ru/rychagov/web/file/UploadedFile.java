package ru.rychagov.web.file;

import org.springframework.web.multipart.MultipartFile;

/**
 * User: Anton Rychagov (ram)
 * Date: 16.11.13 - 16:26
 */
public class UploadedFile {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}