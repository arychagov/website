package ru.rychagov.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Map;

/**
 * User: Anton Rychagov (ram)
 * Date: 16.11.13 - 15:48
 */
@Controller
@RequestMapping("/img/")
public class ShowImageController {
    public static final String UPLOAD_DIRECTORY = "/home/ram/images/";

    @RequestMapping(value = "/{filename:.+}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> showImage(@PathVariable("filename") String filename, HttpServletRequest request) {
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.IMAGE_JPEG);

        final String destination = UPLOAD_DIRECTORY + "/" + filename;

        final File file = new File(destination);
        byte[] fileContent;

        final FileInputStream fin;

        try {
            fin = new FileInputStream(file);

            final BufferedImage image = ImageIO.read(fin);

            final BufferedImage newImage = processImage(image, request);

            final ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ImageIO.write(newImage, "jpg", baos);
            baos.flush();
            fileContent = baos.toByteArray();
            baos.close();
        } catch (FileNotFoundException e) {
            return new ResponseEntity<byte[]>(new byte[0], headers, HttpStatus.CONFLICT);
        } catch (IOException e) {
            return new ResponseEntity<byte[]>(new byte[0], headers, HttpStatus.CONFLICT);
        }

        headers.setContentLength(fileContent.length);

        return new ResponseEntity<byte[]>(fileContent, headers, HttpStatus.OK);
    }

    private BufferedImage processImage(final BufferedImage image, final HttpServletRequest request) {
        BufferedImage img = image;

        try {
            final int w = image.getWidth();
            final int h = image.getHeight();

            img = new BufferedImage(
                    w, h, image.getType());

            Graphics2D graphics = img.createGraphics();
            graphics.drawImage(image, 0, 0, null);

            String text = request.getParameter("t");
            final int x = Integer.parseInt(request.getParameter("x"));
            final int y = Integer.parseInt(request.getParameter("y"));

            Font font = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream("/home/ram/impact.ttf"));

            RenderingHints rh =
                    new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);

            rh.put(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);

            graphics.setRenderingHints(rh);

            if (request.getParameter("s") != null) {
                font = font.deriveFont((float) Integer.parseInt(request.getParameter("s")));
            } else {
                font = font.deriveFont(36F);
            }

            graphics.setFont(font);
            graphics.setColor(Color.WHITE);

            byte[] utf8 = text.getBytes("UTF-8");
            text = new String(utf8);

            graphics.drawString(text, x, y);
        } catch (FontFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return img;
    }
}
